﻿#include <cmath>
#include <iostream>
#include <assert.h>

using namespace std;

int sum(double fractional_number) {
	int sum_number = 0;
	for (int i = 0; i < 3; i++) {
		fractional_number *= 10;
		sum_number += static_cast<int>(fractional_number) % 10;
	}
	if (sum_number < 0) {
		sum_number = -sum_number;
	}
	return sum_number;
}

int main()
{
	assert(sum(123.123) == 6);
	assert(sum(-123.123) == 6);
	assert(sum(123.12) == 3);
	assert(sum(123.1234) == 6);
	assert(sum(0) == 0);
	assert(sum(0.122) == 5);

	double fractional_number;
	cout << "Input number" << endl;
	cin >> fractional_number;
	cout << "Summ number=" << sum(fractional_number) << endl;
	cin.get();
	cin.get();
	return 0;
}